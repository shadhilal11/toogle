function togglePages() {
    var page1 = document.getElementById('page1');
    var page2 = document.getElementById('page2');

    if (page1.style.transform === 'translateY(0%)') {
        page1.style.transform = 'translateY(-100%)';
        page2.style.transform = 'translateY(0%)';
    } else {
        page1.style.transform = 'translateY(0%)';
        page2.style.transform = 'translateY(100%)';
    }
}